package com.kwazart.javafx.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MySQLConnectionUtil {
	public static Connection getConnection() {
		// путь к БД желательно выносить в отдельный файл настроек
		String url = "jdbc:mysql://localhost:3306/addressbook";
		String user = "root";
		String password = "root1234";

		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			return DriverManager.getConnection(url, user, password);
		} catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
			Logger.getLogger(MySQLConnectionUtil.class.getName()).log(Level.SEVERE, null, ex);
		}

		return null;
	}
}
