package com.kwazart.javafx.dao;

import com.kwazart.javafx.objects.Person;
import javafx.collections.ObservableList;

public interface AddressBookDAO {

	boolean add(Person person);

	boolean update(Person person);

	boolean delete(Person person);

	ObservableList<Person> findAll();

	ObservableList<Person> find(String text);
}
