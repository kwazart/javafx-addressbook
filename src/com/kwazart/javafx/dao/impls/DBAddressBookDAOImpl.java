package com.kwazart.javafx.dao.impls;

import com.kwazart.javafx.dao.AddressBookDAO;
import com.kwazart.javafx.db.MySQLConnectionUtil;
import com.kwazart.javafx.objects.Person;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

// класс реализовывает интерфейс с помощью коллекции
public class DBAddressBookDAOImpl implements AddressBookDAO {

	private ObservableList<Person> personList = FXCollections.observableArrayList();

	@Override
	public boolean add(Person person) {
		try (Connection con = MySQLConnectionUtil.getConnection(); PreparedStatement statement = con.prepareStatement("insert into person(fio, phone) values (?, ?)", Statement.RETURN_GENERATED_KEYS)) {
			statement.setString(1, person.getFio());
			statement.setString(2, person.getPhone());

			int result = statement.executeUpdate();
			if (result > 0) {
				ResultSet genKey = statement.getGeneratedKeys();
				int id;
				if (genKey.next()) {
					id = genKey.getInt(1);
					person.setId(id);
				}
				personList.add(person);
				return true;
			}
		} catch (SQLException ex) {
			Logger.getLogger(DBAddressBookDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
		}

		return false;
	}

	@Override
	public boolean delete(Person person) {

		try (Connection con = MySQLConnectionUtil.getConnection(); Statement statement = con.createStatement();) {
			int result = statement.executeUpdate("delete from person where id=" + person.getId());

			if (result > 0) {
				personList.remove(person);
				return true;
			}

		} catch (SQLException ex) {
			Logger.getLogger(DBAddressBookDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
		}

		return false;
	}

	@Override
	public ObservableList<Person> findAll() {

		try (Connection con = MySQLConnectionUtil.getConnection(); Statement statement = con.createStatement(); ResultSet rs = statement.executeQuery("select * from person");) {
			while (rs.next()) {
				Person person = new Person();
				person.setId(rs.getInt("id"));
				person.setFio(rs.getString("fio"));
				person.setPhone(rs.getString("phone"));
				personList.add(person);
			}
		} catch (SQLException ex) {
			Logger.getLogger(DBAddressBookDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
		}

		return personList;
	}

	@Override
	public boolean update(Person person) {
		try (Connection con = MySQLConnectionUtil.getConnection(); PreparedStatement statement = con.prepareStatement("update person set fio=?, phone=? where id=?")) {
			statement.setString(1, person.getFio());
			statement.setString(2, person.getPhone());
			statement.setInt(3, person.getId());

			int result = statement.executeUpdate();
			if (result > 0) {
				// обновление в коллекции происходит автоматически, после нажатия ОК в окне редактирования
				return true;
			}
		} catch (SQLException ex) {
			Logger.getLogger(DBAddressBookDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
		}

		return false;
	}

	@Override
	public ObservableList<Person> find(String text) {

		personList.clear();

		try (Connection con = MySQLConnectionUtil.getConnection();
		     PreparedStatement statement = con.prepareStatement("select * from person where fio like ? or phone like ?");) {

			String searchStr = "%" + text + "%";

			statement.setString(1, searchStr);
			statement.setString(2, searchStr);

			ResultSet rs = statement.executeQuery();

			while (rs.next()) {
				Person person = new Person();
				person.setId(rs.getInt("id"));
				person.setFio(rs.getString("fio"));
				person.setPhone(rs.getString("phone"));
				personList.add(person);
			}
		} catch (SQLException ex) {
			Logger.getLogger(DBAddressBookDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
		}

		return personList;
	}

	public ObservableList<Person> getPersonList() {
		return personList;
	}
}