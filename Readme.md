<h1>ADDRESS BOOK</h1>

<body>
<p>
    The Address Book application is based on Java 8.
    <br>
    The main focus is on using JavaFX.
    <br>
</p>
<p>
    Also, the program uses libraries:
    <br>- Controlsfx-8.20.8
    <br>- Openjfx-dialogs-1.0.2
    <br>- mysql-connector-java-5.1.49
</p>
<p>
    The program has the functions of adding, deleting and changing contacts. In addition, the function of finding the necessary contacts has been implemented.
</p>
<p>
    A distinctive feature can be considered the localization of the program into 2 languages: Russian and English, with a choice.
</p>
<p>
    Do not forget that using the application without manipulating the JRE is only possible on Java 8 (tested on jdk1.8.0_271). In all other cases, after importing the required libraries, you need to add launch parameters.
</p>
<p>
    Version 1.0: data storage is implemented using a collection.
    <br>
    Version 2.0: data storage is implemented using a MySQL database.
    <br>
</p>
</body>

<br><br>

<h1>АДРЕСНАЯ КНИГА</h1>

<body>
<p>
    Приложение Address Book реализовано на Java 8.
    <br>
    Основное внимание уделяется использованию JavaFX.
    <br>
</p>
<p>
    Также в программе используются библиотеки:
    <br>- Controlsfx-8.20.8
    <br>- Openjfx-dialogs-1.0.2
    <br>- mysql-connector-java-5.1.49
</p>
<p>
    В программе есть функции добавления, удаления и изменения контактов. Кроме того, реализована функция поиска необходимых контактов.
</p>
<p>
    Отличительной особенностью можно считать локализацию программы на 2 языка: русский и английский, с возможностью выбора.
</p>
<p>
    Не забывайте, что использовать приложение без манипуляций с JRE возможно только на Ява 8 (протестировано на jdk1.8.0_271). Во всех остальных случая после импортирования необходимых библиотек, необходимо добавить параметры запуска.
</p>
<p>
    Версия 1.0: хранение данных реализовано с помощью коллекции.
    <br>
    Версия 2.0: хранение данных реализовано с помощью базы данных MySQL.
    <br>
</p>
</body>